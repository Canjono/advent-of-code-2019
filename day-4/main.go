package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func main() {
	partOne()
	partTwo()
}

func partOne() {
	ranges := getRanges()
	acceptedPasswords := getAcceptedPasswords(ranges[0], ranges[1], false)

	fmt.Println(len(acceptedPasswords))
}

func partTwo() {
	ranges := getRanges()
	acceptedPasswords := getAcceptedPasswords(ranges[0], ranges[1], true)

	fmt.Println(len(acceptedPasswords))
}

func getRanges() []int {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		panic(err)
	}

	rangesAsStrings := strings.Split(string(input), "-")
	var ranges []int

	for _, rangeAsString := range rangesAsStrings {
		rangeAsInt, _ := strconv.Atoi(rangeAsString)
		ranges = append(ranges, rangeAsInt)
	}

	return ranges
}

func getAcceptedPasswords(lowest int, highest int, useStrictRules bool) []int {
	acceptedPasswords := make([]int, 0)

	for i := lowest; i <= highest; i++ {
		numAsString := strconv.Itoa(i)
		foundDouble := false
		neverDecrease := true

		for j := 1; j < 6; j++ {
			lastNum := numAsString[j-1]
			currentNum := numAsString[j]

			if currentNum < lastNum {
				neverDecrease = false
				break
			}

			if !foundDouble && currentNum == lastNum {
				if useStrictRules {
					foundDouble = onlySameAsOneBehind(j, numAsString) && nextIsNotSame(j, numAsString)
				} else {
					foundDouble = true
				}
			}
		}

		if foundDouble && neverDecrease {
			acceptedPasswords = append(acceptedPasswords, i)
		}
	}

	return acceptedPasswords
}

func onlySameAsOneBehind(i int, number string) bool {
	return i <= 1 || number[i] != number[i-2]
}

func nextIsNotSame(i int, number string) bool {
	return i == 5 || number[i] != number[i+1]
}
