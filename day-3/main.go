package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"strconv"
	"strings"
)

type wire struct {
	paths []path
}

type path struct {
	direction string
	distance  int
}

type position struct {
	x int
	y int
}

func main() {
	partOne()
	partTwo()
}

func partOne() {
	wires := getWires()

	wire1 := wire{paths: getPaths(wires[0])}
	wire1Positions := getAllPositions(wire1)

	wire2 := wire{paths: getPaths(wires[1])}
	wire2Positions := getAllPositions(wire2)

	crossedPositions := getCrossedPositions(wire1Positions, wire2Positions)
	closestIntersection := getClosestIntersection(crossedPositions)

	fmt.Println(closestIntersection)
}

func partTwo() {
	wires := getWires()

	wire1 := wire{paths: getPaths(wires[0])}

	wire2 := wire{paths: getPaths(wires[1])}

	crossedPositions := getCrossedPositions(getAllPositions(wire1), getAllPositions(wire2))

	wire1Positions := getAllPositionsInOrder(wire1)
	wire2Positions := getAllPositionsInOrder(wire2)

	wire1Steps, wire2Steps := getSteps(crossedPositions, wire1Positions, wire2Positions)
	fewestCombinedSteps := getFewestCombinedSteps(wire1Steps, wire2Steps)

	fmt.Println(fewestCombinedSteps)
}

func getWires() []string {
	file, err := os.Open("input.txt")
	if err != nil {
		panic(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	var wires []string

	for scanner.Scan() {
		wires = append(wires, scanner.Text())
	}

	return wires
}

func getPaths(wire string) []path {
	pathsAsStrings := strings.Split(string(wire), ",")
	paths := make([]path, len(pathsAsStrings))

	for i, pathAsString := range pathsAsStrings {
		direction := pathAsString[:1]
		distance, _ := strconv.Atoi(pathAsString[1:])
		path := path{direction: direction, distance: distance}
		paths[i] = path
	}

	return paths
}

func getAllPositions(wire wire) map[int][]int {
	allPositions := make(map[int][]int)
	currentPos := position{x: 0, y: 0}

	for _, path := range wire.paths {
		travelledPositions, newPos := getTravelledPositions(currentPos, path.direction, path.distance)
		for _, position := range travelledPositions {
			if path.direction == "U" || path.direction == "D" {
				_, ok := allPositions[newPos.x]
				if !ok {
					allPositions[newPos.x] = make([]int, 0)
				}
				allPositions[newPos.x] = append(allPositions[newPos.x], position.y)
			} else {
				_, ok := allPositions[position.x]
				if !ok {
					allPositions[position.x] = make([]int, 0)
				}
				allPositions[position.x] = append(allPositions[position.x], newPos.y)
			}
		}
		currentPos = newPos
	}

	return allPositions
}

func getAllPositionsInOrder(wire wire) []position {
	allPositions := make([]position, 0)
	currentPos := position{x: 0, y: 0}

	for _, path := range wire.paths {
		travelledPositions, newPos := getTravelledPositions(currentPos, path.direction, path.distance)
		for _, pos := range travelledPositions {
			allPositions = append(allPositions, pos)
		}
		currentPos = newPos
	}

	return allPositions
}

func getTravelledPositions(currentPos position, direction string, distance int) ([]position, position) {
	travelledPositions := make([]position, 0)

	switch direction {
	case "U":
		goalPosY := currentPos.y - distance
		for i := currentPos.y - 1; i >= goalPosY; i-- {
			position := position{x: currentPos.x, y: i}
			travelledPositions = append(travelledPositions, position)
		}
		currentPos.y = goalPosY
	case "R":
		goalPosX := currentPos.x + distance
		for i := currentPos.x + 1; i <= goalPosX; i++ {
			position := position{x: i, y: currentPos.y}
			travelledPositions = append(travelledPositions, position)
		}
		currentPos.x = goalPosX
	case "D":
		goalPosY := currentPos.y + distance
		for i := currentPos.y + 1; i <= goalPosY; i++ {
			position := position{x: currentPos.x, y: i}
			travelledPositions = append(travelledPositions, position)
		}
		currentPos.y = goalPosY
	case "L":
		goalPosX := currentPos.x - distance
		for i := currentPos.x - 1; i >= goalPosX; i-- {
			position := position{x: i, y: currentPos.y}
			travelledPositions = append(travelledPositions, position)
		}
		currentPos.x = goalPosX
	}

	return travelledPositions, currentPos
}

func getCrossedPositions(wire1Positions map[int][]int, wire2Positions map[int][]int) []position {
	crossedPositions := make([]position, 0)

	for wire1XPos, wire1YPositions := range wire1Positions {
		wire2YPositions, ok := wire2Positions[wire1XPos]

		if !ok {
			continue
		}

		for _, wire1YPos := range wire1YPositions {
			for _, wire2YPos := range wire2YPositions {
				if wire1YPos == wire2YPos {
					crossedPositions = append(crossedPositions, position{x: wire1XPos, y: wire1YPos})
				}
			}
		}
	}

	return crossedPositions
}

func getClosestIntersection(crossedPositions []position) float64 {
	closestIntersection := 10000000.0

	for _, position := range crossedPositions {
		manhattanDistance := math.Abs(float64(position.x)) + math.Abs(float64(position.y))
		if manhattanDistance < closestIntersection {
			closestIntersection = manhattanDistance
		}
	}

	return closestIntersection
}

func getSteps(crossedPositions []position, wire1Positions []position, wire2Positions []position) ([]int, []int) {
	wire1Steps := make([]int, 0)
	wire2Steps := make([]int, 0)

	for _, crossedPos := range crossedPositions {
		for i, wirePos := range wire1Positions {
			if i == 0 {
				continue
			}
			if wirePos.x == crossedPos.x && wirePos.y == crossedPos.y {
				wire1Steps = append(wire1Steps, i+1)
				break
			}
		}

		for i, wirePos := range wire2Positions {
			if i == 0 {
				continue
			}

			if wirePos.x == crossedPos.x && wirePos.y == crossedPos.y {
				wire2Steps = append(wire2Steps, i+1)
				break
			}
		}
	}

	return wire1Steps, wire2Steps
}

func getFewestCombinedSteps(wire1Steps []int, wire2Steps []int) int {
	fewestSteps := 10000000

	for i, wire1Step := range wire1Steps {
		totalSteps := wire1Step + wire2Steps[i]
		if totalSteps < fewestSteps {
			fewestSteps = totalSteps
		}
	}

	return fewestSteps
}
