package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
)

const (
	inputFile     = "input.txt"
	positionMode  = 0
	immediateMode = 1
)

func main() {
	partOne()
	partTwo()
}

func partOne() {
	instructions := getInstructions()
	runProgram(instructions)
}

func partTwo() {
	instructions := getInstructions()
	runProgram(instructions)
}

func getInstructions() []int {
	input, err := ioutil.ReadFile(inputFile)
	if err != nil {
		panic(err)
	}

	instructionsAsStrings := strings.Split(string(input), ",")
	var instructions []int

	for _, instructionAsString := range instructionsAsStrings {
		instruction, _ := strconv.Atoi(instructionAsString)
		instructions = append(instructions, instruction)
	}

	return instructions
}

func runProgram(instructions []int) {
	jump := 0

	for i := 0; i < len(instructions); i = i + jump {
		switch opCode := instructions[i] % 100; opCode {
		case 1:
			add(i, instructions)
			jump = 4
		case 2:
			multiply(i, instructions)
			jump = 4
		case 3:
			takeInput(i, instructions)
			jump = 2
		case 4:
			printOutput(i, instructions)
			jump = 2
		case 5:
			i = jumpIfTrue(i, instructions)
			jump = 0
		case 6:
			i = jumpIfFalse(i, instructions)
			jump = 0
		case 7:
			lessThan(i, instructions)
			jump = 4
		case 8:
			equals(i, instructions)
			jump = 4
		case 99:
			return
		default:
			fmt.Println(strconv.Itoa(opCode) + " is not a valid opCode")
			return
		}
	}
}

func add(startPos int, instructions []int) {
	firstParam, secondParam, thirdParam := getParamValues(startPos, 3, instructions)
	instructions[thirdParam] = firstParam + secondParam
}

func multiply(startPos int, instructions []int) {
	firstParam, secondParam, thirdParam := getParamValues(startPos, 3, instructions)
	instructions[thirdParam] = firstParam * secondParam
}

func jumpIfTrue(startPos int, instructions []int) int {
	firstParam, secondParam, _ := getParamValues(startPos, 2, instructions)

	if firstParam != 0 {
		return secondParam
	}

	return startPos + 3
}

func jumpIfFalse(startPos int, instructions []int) int {
	firstParam, secondParam, _ := getParamValues(startPos, 2, instructions)

	if firstParam == 0 {
		return secondParam
	}

	return startPos + 3
}

func lessThan(startPos int, instructions []int) {
	firstParam, secondParam, thirdParam := getParamValues(startPos, 3, instructions)

	if firstParam < secondParam {
		instructions[thirdParam] = 1
	} else {
		instructions[thirdParam] = 0
	}
}

func equals(startPos int, instructions []int) {
	firstParam, secondParam, thirdParam := getParamValues(startPos, 3, instructions)

	if firstParam == secondParam {
		instructions[thirdParam] = 1
	} else {
		instructions[thirdParam] = 0
	}
}

func takeInput(startPos int, instructions []int) {
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Enter input: ")
	input, err := reader.ReadString('\n')
	if err != nil {
		panic(err)
	}
	number, _ := strconv.Atoi(strings.TrimSpace(input))
	instructions[instructions[startPos+1]] = number
}

func printOutput(startPos int, instructions []int) {
	firstParam, _, _ := getParamValues(startPos, 1, instructions)
	fmt.Println(firstParam)
}

func getParamValues(startPos int, amountOfParams int, instructions []int) (firstParam int, secondParam int, thirdParam int) {
	firstMode, secondMode, _ := getParameterModes(instructions[startPos])

	if amountOfParams < 1 {
		return 0, 0, 0
	}

	if firstMode == immediateMode {
		firstParam = instructions[startPos+1]
	} else {
		firstParam = instructions[instructions[startPos+1]]
	}

	if amountOfParams < 2 {
		return firstParam, 0, 0
	}

	if secondMode == immediateMode {
		secondParam = instructions[startPos+2]
	} else {
		secondParam = instructions[instructions[startPos+2]]
	}

	if amountOfParams < 3 {
		return firstParam, secondParam, 0
	}

	thirdParam = instructions[startPos+3]

	return firstParam, secondParam, thirdParam
}

func getParameterModes(opCode int) (firstMode int, secondMode int, thirdMode int) {
	instruction := strconv.Itoa(opCode)
	instructionLength := len(instruction)

	if instructionLength >= 3 {
		firstMode, _ = strconv.Atoi(string(instruction[instructionLength-3]))
	} else {
		firstMode = 0
	}

	if instructionLength >= 4 {
		secondMode, _ = strconv.Atoi(string(instruction[instructionLength-4]))
	} else {
		secondMode = 0
	}

	if instructionLength >= 5 {
		thirdMode, _ = strconv.Atoi(string(instruction[instructionLength-5]))
	} else {
		thirdMode = 0
	}

	return firstMode, secondMode, thirdMode
}
