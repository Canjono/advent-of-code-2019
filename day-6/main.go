package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

const (
	inputFile = "input.txt"
	me        = "YOU"
	san       = "SAN"
	com       = "COM"
)

func main() {
	partOne()
	partTwo()
}

func partOne() {
	orbits := getOrbits()
	orbitsMap := getOrbitsMap(orbits)
	totalNumberOfOrbits := getTotalNumberOfOrbits(orbitsMap)

	fmt.Println(totalNumberOfOrbits)
}

func partTwo() {
	orbits := getOrbits()
	orbitsMap := getOrbitsMap(orbits)
	minimumTransfers := getMinimumOrbitalTransfers(orbitsMap)

	fmt.Println(minimumTransfers)
}

func getOrbits() []string {
	file, err := os.Open(inputFile)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	var orbits []string

	for scanner.Scan() {
		orbit := scanner.Text()
		orbits = append(orbits, orbit)
	}

	return orbits
}

func getOrbitsMap(orbits []string) map[string]string {
	orbitsMap := make(map[string]string)

	for _, orbit := range orbits {
		objects := strings.Split(orbit, ")")
		orbitsMap[objects[1]] = objects[0]
	}

	return orbitsMap
}

func getTotalNumberOfOrbits(orbitsMap map[string]string) int {
	totalNumber := 0

	for _, target := range orbitsMap {
		currentTarget := target
		for currentTarget != com {
			totalNumber++
			currentTarget = orbitsMap[currentTarget]
		}
		totalNumber++
	}

	return totalNumber
}

func getNumberOfOrbitalTransfers(orbitsMap map[string]string, from string, target string) int {
	currentTarget := orbitsMap[from]
	counter := 0

	for currentTarget != com {
		if currentTarget == target {
			return counter
		}
		counter++
		currentTarget = orbitsMap[currentTarget]
	}

	return -1
}

func getMinimumOrbitalTransfers(orbitsMap map[string]string) int {
	minimumTransfers := -1
	myCounter := 0
	currentTarget := orbitsMap[me]

	for currentTarget != com {
		orbitalTransfersFromSans := getNumberOfOrbitalTransfers(orbitsMap, san, currentTarget)
		if orbitalTransfersFromSans != -1 {
			minimumTransfers = myCounter + orbitalTransfersFromSans
			break
		}
		myCounter++
		currentTarget = orbitsMap[currentTarget]
	}

	return minimumTransfers
}
