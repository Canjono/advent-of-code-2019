package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	partOne()
	partTwo()
}

func partOne() {
	masses := getMasses()
	sum := 0

	for _, mass := range masses {
		sum += getRequiredFuel(mass)
	}

	fmt.Println(sum)
}

func partTwo() {
	masses := getMasses()
	sum := 0

	for _, mass := range masses {
		sum += getTotalFuel(mass)
	}

	fmt.Println(sum)
}

func getMasses() []int {
	file, err := os.Open("input.txt")
	checkError(err)
	defer file.Close()

	scanner := bufio.NewScanner(file)
	var masses []int

	for scanner.Scan() {
		mass, _ := strconv.Atoi(scanner.Text())
		masses = append(masses, mass)
	}

	return masses
}

func checkError(e error) {
	if e != nil {
		panic(e)
	}
}

func getRequiredFuel(mass int) int {
	return mass/3 - 2
}

func getTotalFuel(mass int) int {
	totalFuel := 0

	for true {
		requiredFuel := getRequiredFuel(mass)

		if requiredFuel <= 0 {
			break
		}

		totalFuel += requiredFuel
		mass = requiredFuel
	}

	return totalFuel
}
