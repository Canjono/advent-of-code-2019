package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
)

const (
	inputFile = "input.txt"
	width     = 25
	height    = 6
)

func main() {
	partOne()
	partTwo()
}

func partOne() {
	pixels := getPixels()
	layers := getLayers(pixels, width, height)
	layerWithLeastZeroes := getLayerWithLeastZeroes(layers)
	ones := getAmountOfDigit(layerWithLeastZeroes, 1)
	twos := getAmountOfDigit(layerWithLeastZeroes, 2)

	fmt.Println(ones * twos)
}

func partTwo() {
	pixels := getPixels()
	layers := getLayers(pixels, width, height)
	finalImage := getFinalImage(layers)

	printImage(finalImage)
}

func getPixels() []int {
	input, err := ioutil.ReadFile(inputFile)
	if err != nil {
		panic(err)
	}

	pixelsAsString := string(input)
	var pixels []int

	for _, pixelAsString := range pixelsAsString {
		pixel, _ := strconv.Atoi(string(pixelAsString))
		pixels = append(pixels, pixel)
	}

	return pixels
}

func getLayers(pixels []int, width int, height int) [][]int {
	pixelsPerLayer := width * height
	amountOfLayers := len(pixels) / pixelsPerLayer
	layers := make([][]int, 0)

	for i := 0; i < amountOfLayers; i++ {
		layer := pixels[i*pixelsPerLayer : pixelsPerLayer*(i+1)]
		layers = append(layers, layer)
	}

	return layers
}

func getLayerWithLeastZeroes(layers [][]int) []int {
	layerIndex := -1
	lowestFoundAmount := 10000

	for i, layer := range layers {
		amountOfZeroes := 0
		for _, number := range layer {
			if number == 0 {
				amountOfZeroes++
			}
		}

		if amountOfZeroes < lowestFoundAmount {
			layerIndex = i
			lowestFoundAmount = amountOfZeroes
		}
	}

	return layers[layerIndex]
}

func getAmountOfDigit(layer []int, searchDigit int) int {
	amount := 0

	for _, digit := range layer {
		if digit == searchDigit {
			amount++
		}
	}

	return amount
}

func getFinalImage(layers [][]int) []int {
	finalImage := make([]int, width*height)

	for i := 0; i < width*height; i++ {
		for _, layer := range layers {
			if layer[i] != 2 {
				finalImage[i] = layer[i]
				break
			}
		}
	}

	return finalImage
}

func printImage(image []int) {
	for i := 0; i < len(image); i++ {
		if i%width == 0 {
			fmt.Println("")
		}

		if image[i] == 1 {
			fmt.Print("|")
		} else {
			fmt.Print(" ")
		}
	}
}
