package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func main() {
	partOne()
	partTwo()
}

func partOne() {
	instructions := getInstructions()
	noun := 12
	verb := 2
	output := getOutput(noun, verb, instructions)

	fmt.Println(output)
}

func partTwo() {
	originalInstructions := getInstructions()
	correctNoun := -1
	correctVerb := -1

	for noun := 0; noun < 100; noun++ {
		for verb := 0; verb < 100; verb++ {
			output := getOutput(noun, verb, originalInstructions)
			if output == 19690720 {
				correctNoun = noun
				correctVerb = verb
				break
			}
		}
		if correctNoun != -1 {
			break
		}
	}

	fmt.Println(100*correctNoun + correctVerb)
}

func getInstructions() []int {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		panic(err)
	}

	instructionsAsStrings := strings.Split(string(input), ",")
	var instructions []int

	for _, instructionAsString := range instructionsAsStrings {
		instruction, _ := strconv.Atoi(instructionAsString)
		instructions = append(instructions, instruction)
	}

	return instructions
}

func getOutput(noun int, verb int, originalInstructions []int) int {
	instructions := make([]int, len(originalInstructions))
	copy(instructions, originalInstructions)
	instructions[1] = noun
	instructions[2] = verb

	for i := 0; i < len(instructions); i = i + 4 {
		switch opCode := instructions[i]; opCode {
		case 1:
			add(i, instructions)
		case 2:
			multiply(i, instructions)
		case 99:
			return instructions[0]
		}
	}

	return instructions[0]
}

func add(startPosition int, instructions []int) {
	value1, value2, replacePos := getValuesAndReplacePosition(startPosition, instructions)
	instructions[replacePos] = value1 + value2
}

func multiply(startPosition int, instructions []int) {
	value1, value2, replacePos := getValuesAndReplacePosition(startPosition, instructions)
	instructions[replacePos] = value1 * value2
}

func getValuesAndReplacePosition(startPos int, instructions []int) (int, int, int) {
	valuePos1 := instructions[startPos+1]
	valuePos2 := instructions[startPos+2]
	value1 := instructions[valuePos1]
	value2 := instructions[valuePos2]
	replacePos := instructions[startPos+3]

	return value1, value2, replacePos
}
