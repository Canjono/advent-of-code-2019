package main

import (
	"advent-of-code-2019/day-7/computer"
	"fmt"
)

const (
	inputFile = "input.txt"
)

func main() {
	partOne()
}

func partOne() {
	computer := computer.GetIntCodeComputer(inputFile)
	combinations := getCombinations(0, 4)

	highestSignal := 0

	for _, combination := range combinations {
		lastOutput := 0
		for _, number := range combination {
			inputs := []int{number, lastOutput}
			computer.Init(inputs)
			computer.RunProgram()
			lastOutput = computer.GetLastOutput()
		}

		if lastOutput > highestSignal {
			highestSignal = lastOutput
		}
	}

	fmt.Println(highestSignal)
}

func getCombinations(start int, end int) [][]int {
	var combinations [][]int

	for i := start; i <= end; i++ {
		for j := start; j <= end; j++ {
			if j == i {
				continue
			}
			for k := start; k <= end; k++ {
				if k == j || k == i {
					continue
				}
				for l := start; l <= end; l++ {
					if l == k || l == j || l == i {
						continue
					}
					for m := start; m <= end; m++ {
						if m == l || m == k || m == j || m == i {
							continue
						}
						combination := []int{i, j, k, l, m}
						combinations = append(combinations, combination)
					}
				}
			}
		}
	}

	return combinations
}
