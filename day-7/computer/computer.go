package computer

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

// IntCodeComputer used for doing spaceship calculations.
type IntCodeComputer struct {
	inputFile      string
	positionMode   int
	immediateMode  int
	instructions   []int
	output         int
	inputs         []int
	nextInputIndex int
}

// GetIntCodeComputer creates a new IntCode computer.
func GetIntCodeComputer(inputFile string) IntCodeComputer {
	computer := IntCodeComputer{
		inputFile:     inputFile,
		positionMode:  0,
		immediateMode: 1,
		output:        0,
	}
	return computer
}

// Init sets all values to the default ones, plus the provided ones.
func (computer *IntCodeComputer) Init(inputs []int) {
	computer.setInstructions()
	computer.inputs = inputs
	computer.nextInputIndex = 0
}

// GetLastOutput returns the last output from the program.
func (computer *IntCodeComputer) GetLastOutput() int {
	return computer.output
}

// RunProgram runs the program until it halts.
func (computer *IntCodeComputer) RunProgram() {
	jump := 0

	for i := 0; i < len(computer.instructions); i = i + jump {
		switch opCode := computer.instructions[i] % 100; opCode {
		case 1:
			computer.add(i)
			jump = 4
		case 2:
			computer.multiply(i)
			jump = 4
		case 3:
			computer.takeInput(i)
			jump = 2
		case 4:
			computer.printOutput(i)
			jump = 2
		case 5:
			i = computer.jumpIfTrue(i)
			jump = 0
		case 6:
			i = computer.jumpIfFalse(i)
			jump = 0
		case 7:
			computer.lessThan(i)
			jump = 4
		case 8:
			computer.equals(i)
			jump = 4
		case 99:
			return
		default:
			fmt.Println(strconv.Itoa(opCode) + " is not a valid opCode")
			panic(opCode)
		}
	}
}

func (computer *IntCodeComputer) setInstructions() {
	input, err := ioutil.ReadFile(computer.inputFile)
	if err != nil {
		panic(err)
	}

	instructionsAsStrings := strings.Split(string(input), ",")
	var instructions []int

	for _, instructionAsString := range instructionsAsStrings {
		instruction, _ := strconv.Atoi(instructionAsString)
		instructions = append(instructions, instruction)
	}

	computer.instructions = instructions
}

func (computer *IntCodeComputer) add(startPos int) {
	firstParam, secondParam, thirdParam := computer.getParamValues(startPos, 3)
	computer.instructions[thirdParam] = firstParam + secondParam
}

func (computer *IntCodeComputer) multiply(startPos int) {
	firstParam, secondParam, thirdParam := computer.getParamValues(startPos, 3)
	computer.instructions[thirdParam] = firstParam * secondParam
}

func (computer *IntCodeComputer) jumpIfTrue(startPos int) int {
	firstParam, secondParam, _ := computer.getParamValues(startPos, 2)

	if firstParam != 0 {
		return secondParam
	}

	return startPos + 3
}

func (computer *IntCodeComputer) jumpIfFalse(startPos int) int {
	firstParam, secondParam, _ := computer.getParamValues(startPos, 2)

	if firstParam == 0 {
		return secondParam
	}

	return startPos + 3
}

func (computer *IntCodeComputer) lessThan(startPos int) {
	firstParam, secondParam, thirdParam := computer.getParamValues(startPos, 3)

	if firstParam < secondParam {
		computer.instructions[thirdParam] = 1
	} else {
		computer.instructions[thirdParam] = 0
	}
}

func (computer *IntCodeComputer) equals(startPos int) {
	firstParam, secondParam, thirdParam := computer.getParamValues(startPos, 3)

	if firstParam == secondParam {
		computer.instructions[thirdParam] = 1
	} else {
		computer.instructions[thirdParam] = 0
	}
}

func (computer *IntCodeComputer) takeInput(startPos int) {
	input := computer.inputs[computer.nextInputIndex]
	computer.instructions[computer.instructions[startPos+1]] = input
	computer.nextInputIndex++
}

func (computer *IntCodeComputer) printOutput(startPos int) {
	firstParam, _, _ := computer.getParamValues(startPos, 1)
	computer.output = firstParam
}

func (computer *IntCodeComputer) getParamValues(startPos int, amountOfParams int) (firstParam int, secondParam int, thirdParam int) {
	firstMode, secondMode, _ := computer.getParameterModes(computer.instructions[startPos])

	if amountOfParams < 1 {
		return 0, 0, 0
	}

	if firstMode == computer.immediateMode {
		firstParam = computer.instructions[startPos+1]
	} else {
		firstParam = computer.instructions[computer.instructions[startPos+1]]
	}

	if amountOfParams < 2 {
		return firstParam, 0, 0
	}

	if secondMode == computer.immediateMode {
		secondParam = computer.instructions[startPos+2]
	} else {
		secondParam = computer.instructions[computer.instructions[startPos+2]]
	}

	if amountOfParams < 3 {
		return firstParam, secondParam, 0
	}

	thirdParam = computer.instructions[startPos+3]

	return firstParam, secondParam, thirdParam
}

func (computer *IntCodeComputer) getParameterModes(opCode int) (firstMode int, secondMode int, thirdMode int) {
	instruction := strconv.Itoa(opCode)
	instructionLength := len(instruction)

	if instructionLength >= 3 {
		firstMode, _ = strconv.Atoi(string(instruction[instructionLength-3]))
	} else {
		firstMode = 0
	}

	if instructionLength >= 4 {
		secondMode, _ = strconv.Atoi(string(instruction[instructionLength-4]))
	} else {
		secondMode = 0
	}

	if instructionLength >= 5 {
		thirdMode, _ = strconv.Atoi(string(instruction[instructionLength-5]))
	} else {
		thirdMode = 0
	}

	return firstMode, secondMode, thirdMode
}
